import F2 from '../../common/f2-canvas/lib/f2';

let chart1 = null;
let chart2 = null;
let chart3 = null;
let data3 = require('../../common/mock/series-line.js');
let chart4 = null;
let chart5 = null;
let chart6 = null;

Page({
    data: {
        opts: {
            lazyLoad: true // 延迟加载组件
        },
        message: '请点击饼图，观察我的变化'
    },
    onLoad() {
        this.initChart1();
        this.initChart2();
        this.initChart3();
        this.initChart4();
        this.initChart5();
        this.initChart6();
    },
    initChart1: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#line1');
        self.chartComponent.init((canvas, width, height) => {
  

            const data = [{
                date: '2017-06-05',
                value: 11.6,
                tag: 0
            }, {
                date: '2017-06-06',
                value: 12.9,
                tag: 0
            }, {
                date: '2017-06-07',
                value: 13.5,
                tag: 0
            }, {
                date: '2017-06-08',
                value: 8.6,
                tag: 2
            }, {
                date: '2017-06-09',
                value: 7.3,
                tag: 2
            }, {
                date: '2017-06-10',
                value: 8.5,
                tag: 0
            }, {
                date: '2017-06-11',
                value: 7.3,
                tag: 0
            }, {
                date: '2017-06-12',
                value: 6.8,
                tag: 0
            }, {
                date: '2017-06-13',
                value: 9.2,
                tag: 0
            }, {
                date: '2017-06-14',
                value: 13.0,
                tag: 1
            }, {
                date: '2017-06-15',
                value: 24.5,
                tag: 0
            }, {
                date: '2017-06-16',
                value: 13,
                tag: 0
            }, {
                date: '2017-06-17',
                value: 11.5,
                tag: 1
            }, {
                date: '2017-06-18',
                value: 11.1,
                tag: 0
            }, {
                date: '2017-06-19',
                value: 30.9,
                tag: 0
            }, {
                date: '2017-06-20',
                value: 20.6,
                tag: 1
            }, {
                date: '2017-06-21',
                value: 13.7,
                tag: 1
            }, {
                date: '2017-06-22',
                value: 12.8,
                tag: 1
            }, {
                date: '2017-06-23',
                value: 8.5,
                tag: 0
            }, {
                date: '2017-06-24',
                value: 9.4,
                tag: 1
            }, {
                date: '2017-06-25',
                value: 7.1,
                tag: 0
            }, {
                date: '2017-06-26',
                value: 10.6,
                tag: 0
            }, {
                date: '2017-06-27',
                value: 8.4,
                tag: 0
            }, {
                date: '2017-06-28',
                value: 9.3,
                tag: 0
            }, {
                date: '2017-06-29',
                value: 8.5,
                tag: 0
            }, {
                date: '2017-06-30',
                value: 7.3,
                tag: 0
            }];

            chart1 = new F2.Chart({
                el: canvas,
                width,
                height
            });

            chart1.source(data, {
                value: {
                    tickCount: 5,
                    min: 0,
                    formatter: function formatter(val) {
                        return val.toFixed(2) + '%';
                    }
                },
                date: {
                    type: 'timeCat',
                    range: [0, 1],
                    tickCount: 3
                }
            });

            chart1.axis('date', {
                label: function label(text, index, total) {
                    const textCfg = {};
                    if (index === 0) {
                        textCfg.textAlign = 'left';
                    } else if (index === total - 1) {
                        textCfg.textAlign = 'right';
                    }
                    return textCfg;
                }
            });
            chart1.axis('value', {
                label: function label(text, index, total) {
                    const textCfg = {};
                    if (index === 0) {
                        textCfg.textBaseline = 'bottom';
                    } else if (index === total - 1) {
                        textCfg.textBaseline = 'top';
                    }
                    return textCfg;
                }
            });
            chart1.legend({
                custom: true,
                itemWidth: null,
                items: [{
                    name: '买入点',
                    marker: 'circle',
                    fill: '#F35833'
                }, {
                    name: '卖出点',
                    marker: 'circle',
                    fill: '#518DFE'
                }]
            });
            chart1.line().position('date*value').color('#518DFE');
            chart1.point()
                .position('date*value')
                .size('tag', function (val) {
                    return val ? 3 : 0;
                })
                .style('tag', {
                    fill: function fill(val) {
                        if (val === 2) {
                            return '#518DFE';
                        } else if (val === 1) {
                            return '#F35833';
                        }
                    },
                    stroke: '#fff',
                    lineWidth: 1
                });
            chart1.render();
        });
    },
    initChart2: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#column1');
        self.chartComponent.init((canvas, width, height) => {
            const data = [{
                    genre: '支付宝',
                    sold: 275
                },
                {
                    genre: '微信',
                    sold: 115
                },
                {
                    genre: '银行卡',
                    sold: 120
                },
                {
                    genre: '电子钱包',
                    sold: 350
                },
                {
                    genre: '现金',
                    sold: 150
                }
            ]
            chart2 = new F2.Chart({
                el: canvas,
                width,
                height
            });
            chart2.source(data);
            chart2.interval().position('genre*sold').color('genre');;
            chart2.render();
            // 注意：需要把chart return 出来
            return chart2;
        });
    },
    initChart3: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#line2');
        self.chartComponent.init((canvas, width, height) => {
            chart3 = new F2.Chart({
                el: canvas,
                width,
                height
            });
            chart3.source(data3);
            chart3.scale('date', {
                type: 'timeCat',
                tickCount: 3
            });
            chart3.scale('value', {
                tickCount: 5
            });
            chart3.axis('date', {
                label: function label(text, index, total) {
                    // 只显示每一年的第一天
                    const textCfg = {};
                    if (index === 0) {
                        textCfg.textAlign = 'left';
                    } else if (index === total - 1) {
                        textCfg.textAlign = 'right';
                    }
                    return textCfg;
                }
            });
            chart3.tooltip({
                custom: true, // 自定义 tooltip 内容框
                onChange: function onChange(obj) {
                    const legend = chart3.get('legendController').legends.top[0];
                    const tooltipItems = obj.items;
                    const legendItems = legend.items;
                    const map = {};

                    legendItems.forEach(function (item) {
                        map[item.name] = Object.assign({}, item)
                    });
                    tooltipItems.forEach(function (item) {
                        const name = item.name;
                        const value = item.value;
                        if (map[name]) {
                            map[name].value = value;
                        }
                    });
                    legend.setItems(Object.values(map));
                },
                onHide: function onHide() {
                    const legend = chart.get('legendController').legends.top[0];
                    legend.setItems(chart.getLegendItems().country);
                }
            });
            chart3.line().position('date*value').color('type');
            chart3.render();
            return chart3;
        });
    },
    initChart4: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#pie1');
        self.chartComponent.init((canvas, width, height) => {
            chart4 = new F2.Chart({
                el: canvas,
                width,
                height
            });
            const map = {
                支付宝: '40%',
                微信: '20%',
                银行卡: '18%',
                电子钱包: '15%',
                现金: '5%',
                其他: '0.2%'
            };
            const data = [{
                name: '支付宝',
                percent: 0.4,
                a: '1'
            }, {
                name: '微信',
                percent: 0.2,
                a: '1'
            }, {
                name: '银行卡',
                percent: 0.18,
                a: '1'
            }, {
                name: '电子钱包',
                percent: 0.15,
                a: '1'
            }, {
                name: '现金',
                percent: 0.05,
                a: '1'
            }, {
                name: '其他',
                percent: 0.02,
                a: '1'
            }];
            chart4.source(data, {
                percent: {
                    formatter: function formatter(val) {
                        return val * 100 + '%';
                    }
                }
            });
            chart4.legend({
                position: 'right',
                itemFormatter: function itemFormatter(val) {
                    return val + '  ' + map[val];
                }
            });
            chart4.tooltip(false);
            chart4.coord('polar', {
                transposed: true,
                radius: 0.85
            });
            chart4.axis(false);
            chart4.interval()
                .position('a*percent')
                .color('name', ['#1890FF', '#13C2C2', '#2FC25B', '#FACC14', '#F04864', '#8543E0'])
                .adjust('stack')
                .style({
                    lineWidth: 1,
                    stroke: '#fff',
                    lineJoin: 'round',
                    lineCap: 'round'
                })
                .animate({
                    appear: {
                        duration: 1200,
                        easing: 'bounceOut'
                    }
                });

            chart4.render();
            return chart4;
        });
    },
    initChart5: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#pie2');
        self.chartComponent.init((canvas, width, height) => {
            var data = [{
                    name: '芳华',
                    percent: 0.4,
                    a: '1'
                },
                {
                    name: '妖猫传',
                    percent: 0.2,
                    a: '1'
                },
                {
                    name: '机器之血',
                    percent: 0.18,
                    a: '1'
                },
                {
                    name: '心理罪',
                    percent: 0.15,
                    a: '1'
                },
                {
                    name: '寻梦环游记',
                    percent: 0.05,
                    a: '1'
                },
                {
                    name: '其他',
                    percent: 0.12,
                    a: '1'
                }
            ];
            var chart = new F2.Chart({
                el: canvas,
                width,
                height
            });
            chart.source(data, {
                percent: {
                    formatter: function formatter(val) {
                        return val * 100 + '%';
                    }
                }
            });
            chart.legend({
                position: 'right'
            });
            chart.tooltip(false);
            chart.coord('polar', {
                transposed: true,
                radius: 0.85,
                innerRadius: 0.618
            });
            chart.axis(false);
            chart
                .interval()
                .position('a*percent')
                .color('name', ['#1890FF', '#13C2C2', '#2FC25B', '#FACC14', '#F04864', '#8543E0'])
                .adjust('stack')
                .style({
                    lineWidth: 1,
                    stroke: '#fff',
                    lineJoin: 'round',
                    lineCap: 'round'
                });

            chart.interaction('pie-select', {
                cancelable: false, // 不允许取消选中
                animate: {
                    duration: 300,
                    easing: 'backOut'
                },
                onEnd(ev) {
                    const {
                        shape,
                        data,
                        shapeInfo,
                        selected
                    } = ev;
                    if (shape) {
                        if (selected) {
                            self.setData({
                                message: data.name + ': ' + data.percent * 100 + '%'
                            });
                        }
                    }
                }
            });

            chart.render();
            chart5 = chart;
            return chart5;
        });
    },
    initChart6: function () {
        const self = this;
        self.chartComponent = self.selectComponent('#pie3');
        self.chartComponent.init((canvas, width, height) => {
            const data = [{
                const: 'const',
                type: '交通',
                money: 51.39
            }, {
                const: 'const',
                type: '饮食',
                money: 356.68
            }, {
                const: 'const',
                type: '生活',
                money: 20.00
            }, {
                const: 'const',
                type: '住房',
                money: 116.53
            }, {
                const: 'const',
                type: '其他',
                money: 100
            }];
            chart6 = new F2.Chart({
                el: canvas,
                width,
                height
            });
            chart6.source(data);
            chart6.coord('polar', {
                transposed: true,
                radius: 0.9,
                innerRadius: 0.5
            });
            chart6.axis(false);
            chart6.legend({
                position: 'top',
                align: 'center',
                marker: 'square'
            });

            chart6.tooltip(false);

            chart6.interval()
                .position('const*money')
                .adjust('stack')
                .color('type', ['#1890FF', '#13C2C2', '#2FC25B', '#FACC14']);
            chart6.pieLabel({
                sidePadding: 30,
                activeShape: true,
                label1: function label1(data) {
                    return {
                        text: '￥' + data.money,
                        fill: '#343434',
                        fontWeight: 'bold'
                    };
                },
                label2: function label2(data) {
                    return {
                        text: data.type,
                        fill: '#999'
                    };
                },
                // onClick: function onClick(ev) {
                //     const data = ev.data;
                //     if (data) {
                //         $('#title').text(data.type);
                //         $('#money').text(data.money);
                //     }
                // }
            });
            chart6.render();
            return chart6;
        });
    },
});