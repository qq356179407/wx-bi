import * as echarts from '../../common/echarts/echarts.js';

const app = getApp();

Page({
    onShareAppMessage: res => {
        return {
            title: 'ECharts 可以在微信小程序中使用啦！',
            path: '/pages/index/index',
            success: function() {},
            fail: function() {}
        }
    },

    onReady: function() {
        // 获取组件
        this.ecComponent = this.selectComponent('#mychart-dom-bar');
    },

    data: {
        ec: {
            // 将 lazyLoad 设为 true 后，需要手动初始化图表
            lazyLoad: true
        },
        isLoaded: false,
        isDisposed: false
    },

    // 点击按钮后初始化图表
    init: function() {
        console.log("test")
        this.ecComponent.init((canvas, width, height) => {
            // 获取组件的 canvas、width、height 后的回调函数
            // 在这里初始化图表
            const chart = echarts.init(canvas, null, {
                width: width,
                height: height
            });
            const option = {
                title: {
                    // text: '测试下面legend的红色区域不应被裁剪',
                    // left: 'center'
                },
                legend: {
                    icon: 'rect',
                    itemHeight: 8,
                    itemWidth: 8,
                    data: ['B1层', 'B2层', 'B3层', 'L1层', 'L2层', 'L3层', 'L4层'], // 图例
                    top: -5,
                    left: 'center',
                    z: 2,
                    textStyle: {
                        fontSize: 10
                    }
                },
                color: ['#43D2E3', '#4089FF', '#8C89FE', '#CB9AFE', '#F3763C', '#FBCB29', '#D2DF2D', '#50C871'],
                series: [{
                    name: '访问来源',
                    type: 'pie',
                    radius: ['40%', '75%'],
                    avoidLabelOverlap: false,
                    label: {
                        show: true,
                        position: 'outside',
                        fontSize: 9,
                        formatter: function(e) {
                            return `${e.name} ${e.percent}%`
                        }
                    },
                    labelLine: {
                        show: true
                    },
                    data: [{
                            value: 335,
                            name: 'B1层'
                        },
                        {
                            value: 310,
                            name: 'B2层'
                        },
                        {
                            value: 234,
                            name: 'B3层'
                        },
                        {
                            value: 135,
                            name: 'L1层'
                        },
                        {
                            value: 1548,
                            name: 'L2层'
                        },
                        {
                            value: 1548,
                            name: 'L3层'
                        },
                        {
                            value: 1548,
                            name: 'L4层'
                        }
                    ]
                }]
            };
            chart.setOption(option);

            // 将图表实例绑定到 this 上，可以在其他成员函数（如 dispose）中访问
            this.chart = chart;

            this.setData({
                isLoaded: true,
                isDisposed: false
            });

            // 注意这里一定要返回 chart 实例，否则会影响事件处理等
            return chart;
        });
    },

    dispose: function() {
        if (this.chart) {
            this.chart.dispose();
        }

        this.setData({
            isDisposed: true
        });
    }
});